#!/bin/bash

# configure some default settings for the build
Default_Settings() {
    export TARGET_ARCH=arm64 
    export OF_MAINTAINER="GKart&VancedGamer" 
    export FOX_VERSION=R11.1 
    export FOX_BUILD_TYPE="Beta" 
    export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER=1 
    export ALLOW_MISSING_DEPENDENCIES=true 
    export LC_ALL="C" 
    export OF_SUPPORT_ALL_BLOCK_OTA_UPDATES=0 
    export OF_DISABLE_MIUI_SPECIFIC_FEATURES=1 
    export OF_DISABLE_MIUI_OTA_BY_DEFAULT=1 
    export FOX_ADVANCED_SECURITY=1 
    export FOX_RECOVERY_INSTALL_PARTITION="/dev/block/platform/hi_mci.0/by-name/recovery" 
    export FOX_RECOVERY_SYSTEM_PARTITION="/dev/block/platform/hi_mci.0/by-name/system" 
    export OF_SKIP_ORANGEFOX_PROCESS=1 
    export OF_DEVICE_WITHOUT_PERSIST=1 
    export OF_FLASHLIGHT_ENABLE=0
    export FOX_VERSION=R11.1_2
    export FOX_USE_NANO_EDITOR=1
    export FOX_DELETE_INITD_ADDON=1
    export OF_DEVICE_WITHOUT_PERSIST=1
    export OF_NO_SAMSUNG_SPECIAL=1
    export FOX_USE_SPECIFIC_MAGISK_ZIP="$(LOCAL_PATH)/magisk/Magisk-v21.4.zip"
}

# build the project
do_build() {
  Default_Settings

  # compile it
  . build/envsetup.sh
  
  lunch omni_alice-eng
  
  mka recoveryimage -j`nproc`
}

# --- main --- #
do_build
#
