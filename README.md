# OrangeFox Device Tree Huawei P8 Lite (alice)

## Specs

|        Component        |          Specification            |
| :---------------------- | :-------------------------------- |
| Chipset                 | Kirin 620 v                       |
| Memory                  | 2 GB                              |
| Storage                 | 16GB                              |
| Battery                 | 2200 mAh (non-removable)          |
| Dimensions              | 143 x 70.6 x 7.7 mm               |
| Display                 | 720 x 1280 pix                    |
| Release Date            | 2015 April                        |

## Device Picture

![Huawei P8Lite](http://cdn2.gsmarena.com/vv/pics/huawei/huawei-p8-lite.jpg "Huawei P8Lite")
